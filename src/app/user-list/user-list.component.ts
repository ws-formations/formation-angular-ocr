import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {User} from '../model/User.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  users: User[];
  userSubscription: Subscription;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    //  souscrit au Subject (userSubject) dans  UserService pour emmetre l'objet user
    // (users: User[]) => {this.users = users;}
    this.userSubscription = this.userService.userSubject.subscribe(
      (users: User[]) => {
        // users recu depuis le subject (userSubject)
        this.users = users;
      }
    );
    // On emet le subject
    this.userService.emitUsers();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

}
