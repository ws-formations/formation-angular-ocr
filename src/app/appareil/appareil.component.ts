import {Component, Input, OnInit} from '@angular/core';
import {AppareilService} from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  // appareilName: string = 'Machine à laver';
  // appareilStatus: string = 'éteint';

  @Input() appareilName: string;
  @Input() appareilStatus: string;
  @Input() indexOffAppareil: number;
  @Input() appareilId: number;

  constructor(private appareilService: AppareilService) {
  }

  ngOnInit(): void {
  }

  getStatus(): string {
    return this.appareilStatus;
  }

  getColor(): string {
    if (this.appareilStatus === 'allumé') {
      return 'green';
    } else if (this.appareilStatus === 'éteint') {
      return 'red';
    }
  }

  // tslint:disable-next-line:align
  onSwitchOn(): void{
    this.appareilService.switchOnOne(this.indexOffAppareil);
  }

  offSwitchOff(): void {
    this.appareilService.switchOffOne(this.indexOffAppareil);
  }

}
