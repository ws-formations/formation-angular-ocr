import { Component, OnInit } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

  appareils: any[];
  appareilSubscription: Subscription;
  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });


  // On utilise l'instance AppareilService injecté dans appModule
  constructor(private appareilService: AppareilService) {
  }
  // cette fonction sera executé au moment de la création de AppComponent par angular et c'est l'exécution du constructeur
  ngOnInit(): void{
    // On sourcrit au subject des appareils de type any
    this.appareilSubscription = this.appareilService.appareilsSubject.subscribe(
      (appareils: any[]) => {
        this.appareils = appareils;
      }
    );
    // En fin on emet la sourcription du subject
    this.appareilService.emitAppareilSubject();
  }

  // On applique L' Event binding ie template vers typscript
  onAllumer(): void {
    this.appareilService.switchOnAll();
  }

  onEteindre(): void {
    if (confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
      this.appareilService.switchOffAll();
    } else {
      return null;
    }
  }

  onSave(): void {
    this.appareilService.saveAppareilsToServer();
  }

  onFetch(): void {
    this.appareilService.getAppareilsFromServer();
  }
}
