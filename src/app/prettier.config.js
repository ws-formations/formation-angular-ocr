module.exports = {
  printWidth: 100,
  singleQuote: true,
  semi: true,
  bracketSpacing: true,
  arrowParens: 'always',
  parser: 'typescript'
};
