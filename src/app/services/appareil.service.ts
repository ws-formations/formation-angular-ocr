import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

/**
 * Subjects : est un Observable qui permet non seulement de réagir à de nouvelles informations, mais également d'en émettre
 * appareilsSubject: Le Subject emettra la liste des appareils quand on le demandera de le faire
 */
@Injectable()
export class AppareilService {
  appareilsSubject = new Subject<any[]>();

  private appareils = [];
  constructor(private httpClient: HttpClient) {
  }

  /**
   * Le Subject (appareilsSubject) va emettre la liste des appareils
   * Next: la methode next va forcer le Subject à emettre ce qu'on lui passe comme argument
   * slice() va emmettre une copy du tableau appareils
   */
  emitAppareilSubject(): void {
    this.appareilsSubject.next(this.appareils.slice());
  }

  /**
   *  this.emitAppareilSubject() permet de faire emmettre le Subject pour que les componet souscrit
   *  puissent voire les changements de facon automatique
   */
  switchOnAll(): void {
    for (const appareil of this.appareils) {
      appareil.status = 'allumé';
    }
    this.emitAppareilSubject(); // souscrire aux changements
  }

  switchOffAll(): void {
    for (const appareil of this.appareils) {
      appareil.status = 'éteint';
      this.emitAppareilSubject(); // souscrire aux changements
    }
  }

  switchOnOne(index: number): void{
    this.appareils[index].status = 'allumé';
    this.emitAppareilSubject();
  }

  switchOffOne(index: number): void {
    this.appareils[index].status = 'éteint';
    this.emitAppareilSubject(); // souscrire aux changements
  }
  getAppareilById(appareilId: number) {
    const appareil = this.appareils.find(
      (appareilObject) => {
        return appareilObject.id === appareilId;
      }
    );
    return appareil;
  }

  addAppareil(name: string, status: string): void {
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
    this.appareils.push(appareilObject);
    // emmettre le Subject
    this.emitAppareilSubject();
  }

  saveAppareilsToServer(): void {
    this.httpClient
      .put('https://http-client-demo-8a34e.firebaseio.com/appareils.json', this.appareils)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  getAppareilsFromServer(): void {
    this.httpClient
      .get<any[]>('https://http-client-demo-8a34e.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          this.appareils = response;
          console.log('response ! : ' + this.appareils);
          this.emitAppareilSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}
