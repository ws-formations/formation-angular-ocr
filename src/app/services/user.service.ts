import {User} from '../model/User.model';
import {Subject} from 'rxjs';

export class UserService {
  private users: User[] = [
    new User('Will', 'Alexander', 'will@will.com', 'Coka Cola', ['coder', 'boire du café'])
  ];
  // un subject qui emettra des un tableau de type USER
  userSubject = new Subject<User[]>();

  /**
   * Une methode qui va emettre une copie du tableau USER
   */
  emitUsers(): void {
    this.userSubject.next(this.users.slice());
  }

  /**
   * Ajouter un USER et emettre le USER
   */
  addUser(user: User): void {
    this.users.push(user);
    this.emitUsers();
  }
}
