import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

/**
 * Une guard est un service qu'Angular exécutera au moment où l'utilisateur essaye de naviguer vers la route sélectionnée
 * Si le guard est appliqué à l'url
 * On appliquera ce guard dans AppModule sur les URL à protéger
 * Exemple l'url /appareils': { path: 'appareils', canActivate: [AuthGuard], component: AppareilViewComponent }
 */
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  /**
   * La mehode canActivate a 2 paramètres : route et state
   * La mehode canActivate peut retourner 3 types: Observable | Promise | boolean
   *
   */
  canActivate(
    route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isAuth) {
      return true; // si le user est authentifié on retourne true
    } else {
      this.router.navigate(['/auth']); // redirection vers la page d'authentification
    }
  }
}
