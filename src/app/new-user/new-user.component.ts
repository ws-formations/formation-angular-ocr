import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../model/User.model';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  userForm: FormGroup;

  /**
   * FormGroup: ReactiveForme Module fonctionne avec FormGroup
   * FormBuilder: outil qui permet des créer des formulaire plus facilement
   */
  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  /**
   * On initialise le formulaire
   * La méthode  group  prend comme argument un objet où les clés correspondent aux noms des contrôles souhaités
   * et les valeurs correspondent aux valeurs par défaut de ces champs.
   * Puisque l'objectif est d'avoir des champs vides au départ, chaque valeur ici correspond au string vide.
   * Validators: Est un outil pour la validation de données dans la méthode réactive
   * ['', Validators.required]: '' signifie que la valeur par defaut est un string
   */
  initForm(): void {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      drinkPreference: ['', Validators.required],
      hobbies: this.formBuilder.array([])  // Ajout dynamique des FormControl ici c'est des hobbies qu'on ajoute
    });
  }

  /**
   * formValue.hobbies ? formValue.hobbies : []
   * Cette ligne verifie s'il existe des hobbies si non ca sera un tableau vide
   */
  onSubmitForm(): void {
    const formValue = this.userForm.value;
    const newUser = new User(
      formValue.firstName,
      formValue.lastName,
      formValue.email,
      formValue.drinkPreference,
      formValue.hobbies ? formValue.hobbies : [] // Ajout dynamique des FormControl ici c'est des hobbies qu'on ajoute
    );
    this.userService.addUser(newUser);
    this.router.navigate(['/users']);
  }

  /**
   * afin d'avoir accès aux  controls  à l'intérieur de l'array,
   * pour des raisons de typage strict liées à TypeScript,
   * il faut créer une méthode qui retourne  hobbies  par la méthode  get()  sous forme de  FormArray
   */
  getHobbies(): FormArray {
    return this.userForm.get('hobbies') as FormArray;
  }

  /**
   * créer la méthode qui permet d'ajouter un  FormControl  à  hobbies,
   * permettant ainsi à l'utilisateur d'en ajouter autant qu'il veut.
   * Vous allez également rendre le nouveau champ requis, afin de ne pas avoir un array de  hobbies  avec des string vides:
   */
  onAddHobby(): void {
    const newHobbyControl = this.formBuilder.control(null, Validators.required);
    this.getHobbies().push(newHobbyControl);
  }
}
