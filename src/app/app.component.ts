import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  secondes: number;
  counterSubscription: Subscription;

  ngOnInit(): void {
    const counter = interval(1000); // Notre observable qui emet un chiffre chaque seconde
    // subscribe prend arg: data = value, error, et fin d'observalble
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!'); // pour ce exple cette ligne ne sera pas exécuter
      }
    );  // notre observer subscribe
  }

  /**
   * Pour éviter les bugs liés aux Observables comme ici un compteur à l'infini
   * Il est conseillé de stocker subscribe dans l'objet Subscription et appliquer unsubscribe()
   * ngOnDestroy():  se déclenche quand le component sera détruit
   */
  ngOnDestroy(): void {
    // éviter les bugs lié à un comportement infini
    this.counterSubscription.unsubscribe();
  }
}
