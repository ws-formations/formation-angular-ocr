import { Component, OnInit } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.scss']
})
export class SingleAppareilComponent implements OnInit {

  name = 'Appareil';
  status = 'Statut';

  constructor(private appareilService: AppareilService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // l'id dans params sera le nom de l'appareil
    const appareilId = this.route.snapshot.params['idAppareil'];
    this.name = this.appareilService.getAppareilById(+appareilId).name;
    this.status = this.appareilService.getAppareilById(+appareilId).status;
  }

}
